<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePinjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjaman', function (Blueprint $table) {
            $table->id('id');
            $table->integer('bunga');
            $table->date('tanggal');
            $table->bigInteger('jumlah_pinjaman');
            $table->integer('jangka_waktu');
            $table->integer('no_nasabah');
            $table->foreign('no_nasabah')->references('no_nasabah')->on('customer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjaman');
    }
}
