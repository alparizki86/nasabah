<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customer', 'CustomerCtrl@index');
Route::post('/customer/create', 'CustomerCtrl@create');
Route::get('/customer/{id}/edit', 'CustomerCtrl@edit');
Route::post('/customer/{id}/update', 'CustomerCtrl@update');
Route::get('/customer/{id}/delete', 'CustomerCtrl@delete');

Route::get('/pinjaman', 'PinjamanCtrl@index');
Route::post('/pinjaman/create', 'PinjamanCtrl@create');
Route::get('/pinjaman/{id}/edit', 'PinjamanCtrl@edit');
Route::post('/pinjaman/{id}/update', 'PinjamanCtrl@update');
Route::get('/pinjaman/{id}/delete', 'PinjamanCtrl@delete');
Route::get('/pinjaman/{no_nasabah}/detail', 'PinjamanCtrl@detail');
