<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerCtrl extends Controller
{
    public function customer(Customer $customer)
    {
        $customers = $customer->all();
        return response([
            'status' => [
                'response' => 200,
                'message' => 'sukses',
            ],
            'data' => $customers
        ], 200);
    }

    public function index()
    {
        $data_customer = Customer::all();
        return view('customer.index', ['data_customer' => $data_customer]);
    }

    public function create(Request $request)
    {
        Customer::create($request->all());
        return redirect('/customer')->with('success', 'Data berhasil diinput!!');
    }

    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('customer/edit', ['customer' => $customer]);
    }

    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer->update($request->all());
        return redirect('/customer')->with('success', 'Data berhasil diubah!!');
    }

    public function delete($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        return redirect('/customer')->with('success', 'Data berhasil dihapus!!');
    }
}
