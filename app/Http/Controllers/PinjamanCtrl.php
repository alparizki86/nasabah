<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pinjaman;
use App\Customer;
use Illuminate\Support\Facades\DB;

class PinjamanCtrl extends Controller
{
    public function index()
    {
        $data_pinjaman = Pinjaman::all();
        $data_customer = Customer::all();
        return view('pinjaman.index', ['data_pinjaman' => $data_pinjaman], ['data_customer' => $data_customer]);
    }

    public function create(Request $request)
    {
        Pinjaman::create($request->all());
        return redirect('/pinjaman')->with('success', 'Data berhasil diinput!!');
    }

    public function edit($id)
    {
        $pinjaman = Pinjaman::find($id);
        return view('pinjaman/edit', ['pinjaman' => $pinjaman]);
    }

    public function update(Request $request, $id)
    {
        $pinjaman = Pinjaman::find($id);
        $pinjaman->update($request->all());
        return redirect('/pinjaman')->with('success', 'Data berhasil diubah!!');
    }

    public function delete($id)
    {
        $pinjaman = Pinjaman::find($id);
        $pinjaman->delete();
        return redirect('/pinjaman')->with('success', 'Data berhasil dihapus!!');
    }

    public function detail($no_nasabah)
    {
        $detail = DB::table('pinjaman')
            ->leftJoin('customer', 'pinjaman.no_nasabah', '=', 'customer.no_nasabah')
            ->where('pinjaman.no_nasabah', '=', $no_nasabah)
            ->get();
        return view('pinjaman/detail', ['detail' => $detail]);
    }
}
