<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
    protected $table = 'pinjaman';
    protected $fillable = ['bunga', 'tanggal', 'jumlah_pinjaman', 'jangka_waktu', 'no_nasabah'];
}
