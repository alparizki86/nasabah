@extends('layouts.master')
@section('content')
@if(session('success'))
<div class="alert alert-success" id="alertSuccess" role="alert">
    {{session('success')}}
</div>
@endif
<div class="row">
    <div class="col-6">
        <h1>Data Nasabah</h1>
    </div>
    <div class="col-6">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">
            Tambah Data Nasabah
        </button>
    </div>
    <table class="table table-hover">
        <tr>
            <th>NO NASABAH</th>
            <th>NAMA</th>
            <th>NO REKENING</th>
            <th>ALAMAT</th>
            <th>EMAIL</th>
            <th>HP</th>
            <th>TEMPAT</th>
            <th>AKSI</th>
        </tr>
        @foreach($data_customer as $customer)
        <tr>
            <td>{{$customer->no_nasabah}}</td>
            <td>{{$customer->nama}}</td>
            <td>{{$customer->no_rekening}}</td>
            <td>{{$customer->alamat}}</td>
            <td>{{$customer->email}}</td>
            <td>{{$customer->hp}}</td>
            <td>{{$customer->tempat}}</td>
            <td>
                <a href="/customer/{{$customer->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <a href="/customer/{{$customer->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus ?')">Delete</a>
            </td>
        </tr>
        @endforeach
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Nasabah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/customer/create" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="inputNo">No Nasabah</label>
                        <input name="no_nasabah" type="text" class="form-control" id="inputNo" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="inputName">Nama</label>
                        <input name="nama" type="text" class="form-control" id="inputName" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="inputNoRekening">No Rekening</label>
                        <input name="no_rekening" type="text" class="form-control" id="inputNoRekening" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="inputAddress">Alamat</label>
                        <textarea name="alamat" type="text" class="form-control" id="inputAddress" aria-describedby="emailHelp"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail1">Email</label>
                        <input name="email" type="email" class="form-control" id="inputEmail1" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="inputHp">Hp</label>
                        <input name="hp" type="text" class="form-control" id="inputHp" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="inputPlace">Tempat</label>
                        <textarea name="tempat" type="text" class="form-control" id="inputPlace" aria-describedby="emailHelp"></textarea>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection