@extends('layouts.master')
@section('content')
<h1>Edit data nasabah</h1>
@if(session('success'))
<div class="alert alert-success" id="alertSuccess" role="alert">
    {{session('success')}}
</div>
@endif
<div class="row">
    <div class="col-lg-12">
        <form action="/customer/{{$customer->id}}/update" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label for="inputNo">No Nasabah</label>
                <input name="no_nasabah" value="{{$customer->no_nasabah}}" type="text" class="form-control" id="inputNo" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="inputName">Nama</label>
                <input name="nama" value="{{$customer->nama}}" type="text" class="form-control" id="inputName" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="inputNoRekening">No Rekening</label>
                <input name="no_rekening" value="{{$customer->no_rekening}}" type="text" class="form-control" id="inputNoRekening" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="inputAddress">Alamat</label>
                <textarea name="alamat" type="text" class="form-control" id="inputAddress" aria-describedby="emailHelp">{{$customer->alamat}}</textarea>
            </div>
            <div class="form-group">
                <label for="inputEmail1">Email</label>
                <input name="email" value="{{$customer->email}}" type="email" class="form-control" id="inputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="inputHp">Hp</label>
                <input name="hp" value="{{$customer->hp}}" type="text" class="form-control" id="inputHp" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="inputPlace">Tempat</label>
                <textarea name="tempat" type="text" class="form-control" id="inputPlace" aria-describedby="emailHelp">{{$customer->tempat}}</textarea>
            </div>
            <button type="submit" class="btn btn-warning">Update</button>
            <a href="/customer" class="btn btn-light">Back</a>
        </form>
    </div>
</div>
@endsection