@extends('layouts.master')
@section('content')
@if(session('success'))
<div class="alert alert-success" id="alertSuccess" role="alert">
    {{session('success')}}
</div>
@endif
<div class="row">
    <div class="col-6">
        <h1>Data Pinjaman</h1>
    </div>
    <div class="col-6">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#modalPinjaman">
            Tambah Data Pinjaman
        </button>
    </div>
    <table class="table table-hover">
        <tr>
            <th>NO NASABAH</th>
            <th>BUNGA (%)</th>
            <th>TANGGAL</th>
            <th>JUMLAH PINJAMAN (Rp)</th>
            <th>JANGKA WAKTU (BULAN)</th>
            <th>AKSI</th>
        </tr>
        @foreach($data_pinjaman as $pinjaman)
        <tr>
            <td>{{$pinjaman->no_nasabah}}</td>
            <td>{{$pinjaman->bunga}}</td>
            <td>{{$pinjaman->tanggal}}</td>
            <td>{{$pinjaman->jumlah_pinjaman}}</td>
            <td>{{$pinjaman->jangka_waktu}}</td>
            <td>
                <a href="/pinjaman/{{$pinjaman->no_nasabah}}/detail" class="btn btn-info btn-sm">Detail</a>
                <a href="/pinjaman/{{$pinjaman->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <a href="/pinjaman/{{$pinjaman->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Yakin mau dihapus ?')">Delete</a>
            </td>
        </tr>
        @endforeach
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="modalPinjaman" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pinjaman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/pinjaman/create" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">No Nasabah</label>
                        <select name="no_nasabah" class="form-control" id="exampleFormControlSelect1">
                            @foreach($data_customer as $customer)
                            <option value="{{$customer->no_nasabah}}">{{$customer->no_nasabah}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputTanggal">Tanggal</label>
                        <input name="tanggal" type="date" class="form-control" id="inputTanggal" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="inputJumlahPinjaman">Jumlah Pinjaman (RP)</label>
                        <input name="jumlah_pinjaman" type="text" class="form-control" id="inputJumlahPinjaman" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="inputJangkaWaktu">Jangka Waktu (Bulan)</label>
                        <input name="jangka_waktu" type="text" class="form-control" id="inputJangkaWaktu" aria-describedby="emailHelp">
                    </div>
                    <div class="form-group">
                        <label for="inputBunga">Bunga (%)</label>
                        <input name="bunga" type="text" class="form-control" id="inputBunga" aria-describedby="emailHelp">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection