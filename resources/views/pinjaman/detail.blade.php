@extends('layouts.master')
@section('content')
<div class="row">
    <div class="col-12">
        <h1>Data Angsuran</h1>
    </div>
    <dl class="col-12">
        <div class="col-12">
            <div class="row">
                <dt class="col-sm-3">No Nasabah</dt>
                <dd class="col-sm-9">: {{$detail[0]->no_nasabah}}</dd>
            </div>

            <div class="row">
                <dt class="col-sm-3">Nama</dt>
                <dd class="col-sm-9">: {{$detail[0]->nama}}</dd>
            </div>
            <div class="row">
                <dt class="col-sm-3">Alamat</dt>
                <dd class="col-sm-9">: {{$detail[0]->alamat}}</dd>
            </div>
            <div class="row">
                <dt class="col-sm-3">Tanggal Pinjaman</dt>
                <dd class="col-sm-9">: {{$detail[0]->tanggal}}</dd>
            </div>
            <div class="row">
                <dt class="col-sm-3">Jumlah Pinjaman</dt>
                <dd class="col-sm-9">: Rp.{{$detail[0]->jumlah_pinjaman}}</dd>
            </div>
            <div class="row">
                <dt class="col-sm-3">Jangka Waktu</dt>
                <dd class="col-sm-9">: {{$detail[0]->jangka_waktu}} Bulan</dd>
            </div>
            <div class="row">
                <dt class="col-sm-3">Bunga</dt>
                <dd class="col-sm-9">: {{$detail[0]->bunga}}%</dd>
            </div>
        </div>
        <table class="table table-hover">
            <tr>
                <th>BULAN</th>
                <th>ANGSURAN POKOK</th>
                <th>BUNGA</th>
                <th>TOTAL</th>
            </tr>
            @php
            $bunga = ($detail[0]->jumlah_pinjaman * $detail[0]->bunga / 100) / $detail[0]->jangka_waktu;
            $pokok = $detail[0]->jumlah_pinjaman / $detail[0]->jangka_waktu;
            $total = $bunga + $pokok;
            @endphp
            @for ($i = 1; $i <= $detail[0]->jangka_waktu; $i++)
                <tr>

                    <td>{{ $i }}</td>
                    <td>{{round($pokok)}}</td>
                    <td>{{$bunga}}</td>
                    <td>{{round($total)}}</td>

                </tr>
                @endfor
        </table>

    </dl>
</div>
@endsection