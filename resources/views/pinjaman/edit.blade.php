@extends('layouts.master')
@section('content')
<h1>Edit data Pinjaman</h1>
@if(session('success'))
<div class="alert alert-success" id="alertSuccess" role="alert">
    {{session('success')}}
</div>
@endif
<div class="row">
    <div class="col-lg-12">
        <form action="/pinjaman/{{$pinjaman->id}}/update" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label for="exampleFormControlSelect1">No Nasabah</label>
                <select name="no_nasabah" class="form-control" id="exampleFormControlSelect1" disabled>
                    <option value="{{$pinjaman->no_nasabah}}">{{$pinjaman->no_nasabah}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="inputTanggal">Tanggal</label>
                <input name="tanggal" value="{{$pinjaman->tanggal}}" type="date" class="form-control" id="inputTanggal" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="inputJumlahPinjaman">Jumlah Pinjaman (RP)</label>
                <input name="jumlah_pinjaman" value="{{$pinjaman->jumlah_pinjaman}}" type="text" class="form-control" id="inputJumlahPinjaman" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="inputJangkaWaktu">Jangka Waktu (Bulan)</label>
                <input name="jangka_waktu" value="{{$pinjaman->jangka_waktu}}" type="text" class="form-control" id="inputJangkaWaktu" aria-describedby="emailHelp">
            </div>
            <div class="form-group">
                <label for="inputBunga">Bunga (%)</label>
                <input name="bunga" value="{{$pinjaman->bunga}}" type="text" class="form-control" id="inputBunga" aria-describedby="emailHelp">
            </div>
            <button type="submit" class="btn btn-warning">Update</button>
            <a href="/pinjaman" class="btn btn-light">Back</a>
        </form>
    </div>
</div>
@endsection